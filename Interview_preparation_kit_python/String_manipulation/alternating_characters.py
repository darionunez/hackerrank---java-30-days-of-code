#!/bin/python3

import math
import os
import random
import re
import sys

# Count how many times a character appears next to the same character
def alternatingCharacters(s):
    deletions = 0

    for c in range(len(s)):
        if (c <= len(s)-2 and s[c] == s[c+1]):
            deletions += 1

    return deletions

if __name__ == '__main__':
    q = int(input())
    for q_itr in range(q):
        s = input()
        result = alternatingCharacters(s)
        print(result)
