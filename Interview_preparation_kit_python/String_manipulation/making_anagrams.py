#!/bin/python3

import math
import os
import random
import re
import sys

def pm(m):
    for e in m:
        print(f"({e} -> {m[e]})", end=" ")
    print()

# 1. Compute frequency maps for each string
# 2. Count frequencies of letters not appearing on the other map
# 3. Count the difference in frequencies of letters appearing on both maps
def makeAnagram(a, b):
    deletions = 0
    mapa = {}
    mapb = {}

    for i in a:
        if (i in mapa.keys()):
            mapa[i] = mapa[i] + 1
        else:
            mapa[i] = 1

    for i in b:
        if (i in mapb.keys()):
            mapb[i] = mapb[i] + 1
        else:
            mapb[i] = 1

    for i in mapa.keys():
        if (i not in mapb.keys()):
            deletions = deletions + mapa[i]
        else:
            deletions = deletions + max(0, mapa[i] - mapb[i])

    for i in mapb.keys():
        if (i not in mapa.keys()):
            deletions = deletions + mapb[i]
        else:
            deletions = deletions + max(0, mapb[i] - mapa[i])

    return deletions

if __name__ == '__main__':
    a = input()
    b = input()
    res = makeAnagram(a, b)
    print(res)
