#!/bin/python3

import math
import os
import random
import re
import sys

def solve(case):
    opening = ['{','[','(']
    closing = ['}',']',')']

    stk = []

    for c in case:
        if (c in opening):
            stk.append(c)
        else:
            if (len(stk) <= 0):
                return "NO"

            newSymbolIndex = closing.index(c)
            topSymbolIndex = opening.index(stk[-1])

            if (newSymbolIndex == topSymbolIndex):
                stk.pop()
            else:
                return "NO"
    
    if (len(stk) > 0):
        return "NO"
        
    return "YES"

# Complete the braces function below.
def braces(values):
    answers = []
    for case in values:
        answers.append(solve(case))
    
    return answers

if __name__ == '__main__':
    values_count = int(input())
    values = []

    for _ in range(values_count):
        values_item = input()
        values.append(values_item)

    res = braces(values)
    print(res)