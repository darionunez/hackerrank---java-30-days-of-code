#!/bin/python3

import math
import os
import random
import re
import sys
import itertools


#
# Complete the 'countOptions' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts following parameters:
#  1. INTEGER people
#  2. INTEGER groups
#

def countOptions(people, groups):
    peopleList = list(range(1, people + 1))
    options = list(itertools.combinations_with_replacement(peopleList, groups))
    goodOptions = []

    # Applying rules
    for option in options:
        if (sum(list(option))) != people:
            continue
        
        brule = True
        for g in range(len(option)):
            if g > 0 and option[g] < option[g - 1]:
                brule = False
        if brule == False:
            continue

        tupleList = list(option)
        tupleList.sort()

        if tupleList in goodOptions:
            continue

        goodOptions.append(tupleList)

    # print(goodOptions)
    return len(goodOptions)

if __name__ == '__main__':
    people = int(input().strip())
    groups = int(input().strip())
    result = countOptions(people, groups)
    print(result)