class Node:
    def __init__(self, info): 
        self.info = info  
        self.left = None  
        self.right = None 
        self.level = None 

    def __str__(self):
        return str(self.info) 

class BinarySearchTree:
    def __init__(self): 
        self.root = None

    def create(self, val):  
        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
         
            while True:
                if val < current.info:
                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break
                elif val > current.info:
                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break

# Enter your code here. Read input from STDIN. Print output to STDOUT
# A DFS solution
# 1. Add all children of a node to the list of nodes, increasing their height by 1. [value, height]
# 2. Remove the current node from the list
# 3. Stop when there's only one element left
# 4. Return the height of that last element
def height(root):
    nodes = [[root, 0]]

    while True:
        currentNode = nodes[0]

        if (currentNode[0].right):
            nodes.append([currentNode[0].right, currentNode[1] + 1])

        if (currentNode[0].left):
            nodes.append([currentNode[0].left, currentNode[1] + 1])

        if (len(nodes) == 1):
            return nodes[0][1]

        nodes.pop(0)

tree = BinarySearchTree()
t = int(input())

arr = list(map(int, input().split()))

for i in range(t):
    tree.create(arr[i])

print(height(tree.root))
