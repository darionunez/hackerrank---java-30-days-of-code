#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Sort prices
# 2. Run through them buying toys until budget is passed
def maximumToys(prices, k):
    runningTotal = 0
    toysCounter = 0
    prices.sort()

    print(prices)

    for p in range(len(prices)):
        if k > (runningTotal + prices[p]):
            toysCounter = toysCounter + 1
            runningTotal = runningTotal + prices[p]
        else:
            break

    return toysCounter

if __name__ == '__main__':
    nk = input().split()
    n = int(nk[0])
    k = int(nk[1])
    prices = list(map(int, input().rstrip().split()))
    result = maximumToys(prices, k)
    print(result)
