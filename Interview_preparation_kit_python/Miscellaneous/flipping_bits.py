#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Convert number to a list of bits
# 2. Swap value of all the bits
# 3. Convert the list back to an integer
def flippingBits(n):
    nBin = list(format(n, '032b'))

    for i in range(len(nBin)):
        if (nBin[i] == '1'):
            nBin[i] = '0'
        else:
            nBin[i] = '1'

    return int(("".join(nBin)), 2)

if __name__ == '__main__':
    q = int(input())

    for q_itr in range(q):
        n = int(input())
        result = flippingBits(n)
        print(result)
