import heapq

li = []
heapq.heapify(li)

q = int(input())

for q_itr in range(q):
    line = input()

    if len(line) > 1:        
        n,m = [int(value) for value in line.split()]
    else:
        n = int(line)

    # Use the heapq library to turn a list into a heap
    # Using heapq functions preserves heap order
    # After performing an unorthodox operation like 'remove', need to re heapify the list
    if (n == 1):
        heapq.heappush(li, m)
    elif (n == 2):
        li.remove(m)
        heapq.heapify(li)
    else:
        print(heapq.nsmallest(1, li)[0])