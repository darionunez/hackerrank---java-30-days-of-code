#!/bin/python3

import math
import os
import random
import re
import sys

def turn(currentDirection, command):
    directions = ["UP", "RIGHT", "DOWN", "LEFT"]

    if (command == "L"):
        newDirectionIndex = (directions.index(currentDirection) - 1) % len(directions)
        print(f"DYNAMIC INDEX: {directions[newDirectionIndex]}")
    else:
        newDirectionIndex = (directions.index(currentDirection) + 1) % len(directions)
        print(f"DYNAMIC INDEX: {directions[newDirectionIndex]}")

    return directions[newDirectionIndex]

def executeCommands(commands):
    startingPos = [0, 0]
    facing = "UP"

    for c in commands:
        if (facing == "UP"):
            if (c == "G"):
                startingPos[0] = startingPos[0] - 1
            elif (c == "L"):
                facing = turn(facing, "L")
            else:
                facing = turn(facing, "R")
        elif (facing == "DOWN"):
            if (c == "G"):
                startingPos[0] = startingPos[0] + 1
            elif (c == "L"):
                facing = turn(facing, "L")
            else:
                facing = turn(facing, "R")
        elif (facing == "LEFT"):
            if (c == "G"):
                startingPos[1] = startingPos[1] - 1
            elif (c == "L"):
                facing = turn(facing, "L")
            else:
                facing = turn(facing, "R")
        else:
            if (c == "G"):
                startingPos[1] = startingPos[1] + 1
            elif (c == "L"):
                facing = turn(facing, "L")
            else:
                facing = turn(facing, "R")

    return [startingPos, facing]

def doesCircleExist(commands):
    answers = []

    for commandArray in commands:
        if (len(commandArray) == 1):
            if (commandArray[0] == "L" or commandArray[0] == "R"):
                answers.append("YES")
            else:
                answers.append("NO")
        else:
            print(executeCommands(commandArray))

            finalPos = executeCommands(commandArray)[0]
            finalFacing = executeCommands(commandArray)[1]

            # If facing UP, there must be an offset in total travelled for it to loop back
            # If not facing UP, it will always go back to the start config
            if (finalFacing == "UP" and (abs((finalPos[0])) + (abs(finalPos[1]))) > 0):
                answers.append("NO")
            else:
                answers.append("YES")

    return answers


if __name__ == '__main__':
    commands_count = int(input().strip())
    commands = []

    for _ in range(commands_count):
        commands_item = input()
        commands.append(commands_item)

    result = doesCircleExist(commands)
    print(result)
