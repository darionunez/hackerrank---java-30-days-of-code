#!/bin/python3

import math
import os
import random
import re
import sys



#
# Complete the 'meanderingArray' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY unsorted as parameter.
#

def meanderingArray(unsorted):
    meandArr = []
    unsorted.sort()

    while True:
        if (len(unsorted) == 1):
            meandArr.append(unsorted[0])
            break
        elif (len(unsorted) == 0):
            break
        else:
            largest = unsorted[-1]
            smallest = unsorted[0]

            meandArr.append(largest)
            meandArr.append(smallest)

            unsorted.pop(-1)
            unsorted.pop(0)

    return meandArr


if __name__ == '__main__':
    unsorted_count = int(input().strip())
    unsorted = []

    for _ in range(unsorted_count):
        unsorted_item = int(input().strip())
        unsorted.append(unsorted_item)

    result = meanderingArray(unsorted)
    print(result)
