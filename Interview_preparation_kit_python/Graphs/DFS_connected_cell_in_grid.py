###########################################################################
# Problem is finding the size of a region of 1s in an array of 1s and 0s
# Can be done with BFS but this problem is more suitable for DFS
###########################################################################

#!/bin/python3

import math
import os
import random
import re
import sys

regions = []
visited = {}

def pg(grid):
    for row in grid:
        print(row)

def getPositiveNeighbours(grid, cell):
    neighbours = []

    for i in range(-1, 2):
        for j in range(-1, 2):
            row = cell[0] - i
            col = cell[1] - j

            if (row >= 0 and row <= n-1 and col >= 0 and col <= m-1):
                if (grid[row][col] == 1 and (row, col) != cell):
                    neighbours.append((row, col))
    
    return neighbours

def generateVisitedMap(grid):
    for i in range(n):
        for j in range(m):
            visited[(i, j)] = False

    return visited

# DFS algorithm on an array
# Key is to insert neighbours at the front of the list
def regionDFS(grid, cell):
    regionCells = []
    unexpanded = [cell]
    visited[cell] = True
    regionCells.append(cell)

    while True:
        current = unexpanded[0]
        unexpanded.pop(0)
        neighbours = getPositiveNeighbours(grid, current)

        for n in neighbours:
            if (visited[n] == False):
                unexpanded.insert(0, n) # insert at start
                visited[n] = True
                regionCells.append(n)

        if (len(unexpanded) < 1):
            break

    return regionCells

def maxRegion(grid):
    regions = []
    visited = generateVisitedMap(grid)

    for i in range(n):
        for j in range(m):
            if (grid[i][j] == 1):
                if (visited[(i, j)]):
                    continue
                else:
                    regions.append(regionDFS(grid, (i, j)))

    regionLengths = list(map(lambda x: len(x), regions))
    return max(regionLengths)

if __name__ == '__main__':
    n = int(input())
    m = int(input())
    grid = []
    for _ in range(n):
        grid.append(list(map(int, input().rstrip().split())))
    res = maxRegion(grid)
    print(res)
