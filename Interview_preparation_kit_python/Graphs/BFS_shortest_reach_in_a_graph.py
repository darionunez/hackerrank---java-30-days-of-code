###########################################################################
# Problem is finding the minimum spanning tree of a graph starting from s
###########################################################################

# n = nodes
# m = edges
# x, y, s are node indices [0-n]
class Graph:
    graph = {}

    def __init__(self, n):
        self.graph = {}
        for val in range(n):
            self.graph[val] = []

    def connect(self, x, y):
        self.graph[x].append(y)
        self.graph[y].append(x)

    def printGraph(self):
        print("#" * 20)
        for k in self.graph.keys():
            print(f"{k} -> {self.graph[k]}")
        print('#' * 20)

    def printDistances(self, visitedNodes, s):
        graphWithoutS = list(filter(lambda x : x != s , self.graph.keys()))
    
        for k in graphWithoutS:
            if (k in visitedNodes.keys()):
                print(visitedNodes[k] * 6, end=" ")
            else:
                print(-1, end=" ")

        print()

    # BFS algorithm the graph
    def find_all_distances(self, s):
        visitedNodes = {}
        unexpandedNodes = [s]
        visitedNodes[s] = 0

        while True:
            currentNode = unexpandedNodes[0]
            unexpandedNodes.pop(0)
            neighbours = self.graph[currentNode]

            for n in neighbours:
                if (n not in visitedNodes.keys()):
                    unexpandedNodes.append(n)   # insert at end
                    visitedNodes[n] = visitedNodes[currentNode] + 1
            
            if (len(unexpandedNodes) < 1):
                break

        self.printDistances(visitedNodes, s)
        return

# HackerRank code
t = int(input())
for i in range(t):
    n,m = [int(value) for value in input().split()]
    graph = Graph(n)
    for i in range(m):
        x,y = [int(x) for x in input().split()]
        graph.connect(x-1,y-1) 
    s = int(input())
    graph.find_all_distances(s-1)