# The rest of the code is hidden

# 1. Store ids of visitied nodes
# 2. If any new node is already in the list, there is a cycle
def has_cycle(head):
    seenNodes = []
    
    while head != None:
        if (id(head) in seenNodes):
            return 1
        seenNodes.append(id(head))
        head = head.next
        
    return 0