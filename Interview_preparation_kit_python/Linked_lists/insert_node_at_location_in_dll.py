#!/bin/python3

import math
import os
import random
import re
import sys

class DoublyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None
        self.prev = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = DoublyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node
            node.prev = self.tail

        self.tail = node

def print_doubly_linked_list(node: DoublyLinkedListNode):
    print("[ ", end="")
    while node:
        if node:
            print(node.data, end=" ")
        node = node.next
    print("]")

# Based on the place of insertion, rearange pointers differently
def sortedInsert(head, data):
    newNode = DoublyLinkedListNode(data)

    # Adding on a empty list
    if (head == None):
        head.data = data
    else:
        currentNode = head
        while True:
            # Adding at the end
            if (currentNode.next == None and data > currentNode.data):
                currentNode.next = newNode
                newNode.prev = currentNode
                break
            # Adding at the front
            elif (data <= currentNode.data and currentNode.prev == None):
                currentNode.prev = newNode
                newNode.next = currentNode
                head = newNode
                break
            # Adding anywhere else
            elif (data <= currentNode.data):
                currentNode.prev.next = newNode
                currentNode.prev = newNode
                newNode.prev = currentNode.prev
                newNode.next = currentNode
                break
            else:
                currentNode = currentNode.next

    return head

if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        llist_count = int(input())
        llist = DoublyLinkedList()

        for _ in range(llist_count):
            llist_item = int(input())
            llist.insert_node(llist_item)

        data = int(input())
        llist1 = sortedInsert(llist.head, data)
        print_doubly_linked_list(llist1)
