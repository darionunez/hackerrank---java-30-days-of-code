#!/bin/python3

import math
import os
import random
import re
import sys

class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node


        self.tail = node

def print_singly_linked_list(node, sep):
    print("[", end="")
    while node:
        if node:
            print(node.data, end=sep)
        node = node.next
    print("]")

# 1. Iterate to the node before the desired position
# 2. Create the new node
# 3. Set new node next to current node next
# 4. Set new current node next to new node
def insertNodeAtPosition(head, data, position):
    if (head == None):
        head.data = data
    else:
        currentNode = head
        for i in range(position-1):
            currentNode = currentNode.next

        newNode: SinglyLinkedListNode = SinglyLinkedListNode(data)
        newNode.next = currentNode.next
        currentNode.next = newNode
    return head

if __name__ == '__main__':
    llist_count = int(input())
    llist = SinglyLinkedList()
    for _ in range(llist_count):
        llist_item = int(input())
        llist.insert_node(llist_item)
    data = int(input())
    position = int(input())
    llist_head = insertNodeAtPosition(llist.head, data, position)
    print_singly_linked_list(llist_head, ' ')