#!/bin/python3

import math
import os
import random
import re
import sys

class DoublyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None
        self.prev = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = DoublyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node
            node.prev = self.tail


        self.tail = node

def print_doubly_linked_list(node: DoublyLinkedListNode):
    print("[ ", end="")
    while node:
        if node:
            print(node.data, end=" ")
        node = node.next
    print("]")

# 1. Load the whole list in an array
# 2. Go through it again assigning opposite values in the array
def reverse(head):
    linkedListValues = []

    cn = head

    while cn != None:
        linkedListValues.append(cn.data)
        cn = cn.next

    cn = head
    while cn != None:
        cn.data = linkedListValues[-1]
        linkedListValues.pop(-1)
        cn = cn.next

    return head

if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        llist_count = int(input())
        llist = DoublyLinkedList()

        for _ in range(llist_count):
            llist_item = int(input())
            llist.insert_node(llist_item)

        llist1 = reverse(llist.head)
        print_doubly_linked_list(llist1)
