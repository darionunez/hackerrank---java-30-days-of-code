#!/bin/python3

import math
import os
import random
import re
import sys

# Use newIndex = (oldIndex - rotations) % arrayLength
def rotLeft(a, d):
    newArray = []
    
    for elem in range(len(a)):
        newIndex = (elem - d) % len(a)
        newArray.insert(newIndex, a[elem])

    return newArray

if __name__ == '__main__':
    nd = input().split()
    n = int(nd[0])
    d = int(nd[1])
    a = list(map(int, input().rstrip().split()))
    result = rotLeft(a, d)
    print(result)
