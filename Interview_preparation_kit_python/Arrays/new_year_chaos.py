#!/bin/python3

import math
import os
import random
import re
import sys
import copy

# Less efficient algorithm
# 1. Move numbers with a negative delta back
# 2. Subtract 1 from any numbers they go over
# 3. Repeat until getting the ordered array (no bribers detected)
def minimumBribesSlow(q):
    numberOfBribes = 0

    for i in range(len(q)):
        if (i - (q[i] - 1) < -2):
            print(f"Too chaotic")
            return

    # deltaQ = list(map(lambda x : [x, (q.index(x) - (x - 1))], copy.deepcopy(q)))
    deltaQ = [[x, (q.index(x) - (x - 1))] for x in q]

    while True:
        # print(f"deltaQ: {deltaQ}")

        bribers = list(filter(lambda x : x[1] < 0, deltaQ))

        if len(bribers) <= 0:
            break

        largestBriber = max(bribers, key=lambda x : x[0])
        largestBriberIndex = deltaQ.index(largestBriber)

        for i in range(largestBriberIndex + 1, largestBriberIndex + abs(largestBriber[1]) + 1):
            deltaQ[i][1] = deltaQ[i][1] - 1

        numberOfBribes = numberOfBribes + abs(largestBriber[1])
        deltaQ.remove(largestBriber)
        deltaQ.insert(largestBriberIndex + abs(largestBriber[1]), largestBriber)
        largestBriber[1] = 0

    print(numberOfBribes)

# Count the number of times all people were bribed
# The key is to understand that:
# A briber Q of a bribee P is a larger number that P found in the range:
# Poriginal-1 and Pcurrent-1. It cannot be found more than 1 position
# ahead of P's original position. It can only be found before P's current
# position.
def minimumBribes(q):
    bribeCount = 0

    for i in range(len(q)):
        if (q[i] - (i + 1) > 2):
            print("Too chaotic")
            return

        iOriginal = (q[i] - 1) - 1
        iCurrent = i - 1

        for j in range(max(0, iOriginal), iCurrent + 1):
            if (q[j] > q[i]):
                bribeCount = bribeCount + 1
    
    print(bribeCount)

if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        n = int(input())
        q = list(map(int, input().rstrip().split()))
        minimumBribes(q)