#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the reverseArray function below.
def reverseArray(a):
    reA = []
    
    for i in range(len(a)-1, -1, -1):
        reA.append(a[i])

    return reA

if __name__ == '__main__':
    arr_count = int(input())
    arr = list(map(int, input().rstrip().split()))
    res = reverseArray(arr)
    print(res)