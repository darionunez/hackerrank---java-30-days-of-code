#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Iterate until i - 2 on both axis
# 2. Calculate the total of all possible hourglasses
# 3. Get the maximum from the list of costs
def hourglassSum(arr):
    costs = []
    for i in range(len(arr)-2):
        for j in range (len(arr)-2):
            top = arr[i][j] + arr[i][j + 1] + arr[i][j + 2]
            middle = arr[i + 1][j + 1]
            bottom = arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2]
            costs.append(top + middle + bottom)

    return max(costs)

if __name__ == '__main__':
    arr = []
    for _ in range(6):
        arr.append(list(map(int, input().rstrip().split())))

    result = hourglassSum(arr)
    print(result)
