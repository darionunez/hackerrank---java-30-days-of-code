#!/bin/python3

import math
import os
import random
import re
import sys

# Brute force search. Iterate through everything
def minimumAbsoluteDifferenceBrute(arr):
    minDif = sys.maxsize
    minPair = []
    for i in range(len(arr)):
        for j in range(len(arr)):
            if (i != j):
                dif = abs(arr[i] - arr[j])

                if dif == 0:
                    return 0

                if (dif < minDif):
                    minDif = dif
                    minPair = [arr[i], arr[j]]
    
    print(minPair)
    return minDif

# 1. Sort the array
# 2. Find the 2 consequent numbers with the least distance between them
def minimumAbsoluteDifference(arr):
    minDif = sys.maxsize
    arr.sort()

    for i in range(len(arr) - 1):
        dif = abs(arr[i] - arr[i+1])

        if dif == 0:
            return 0

        if (dif < minDif):
            minDif = dif

    return minDif

if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().rstrip().split()))
    result = minimumAbsoluteDifference(arr)
    print(result)
