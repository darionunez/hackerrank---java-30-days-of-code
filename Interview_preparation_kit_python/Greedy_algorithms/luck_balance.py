#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Sort array by [0] -> luck in descending AND then by [1] -> important in ascending
# 2. Go through the array adding up luck until k contests are lost
# 3. Subtract the luck of the rest (have to win them)
def luckBalance(k, contests):
    maxLuck = 0
    sortedContests = sorted(contests, key=lambda e: (-e[0], e[1]))

    for contest in range(len(sortedContests)):
        if (k - sortedContests[contest][1]) >= 0:
            k = k - sortedContests[contest][1]
            maxLuck = maxLuck + sortedContests[contest][0]
        else:
            maxLuck = maxLuck - sortedContests[contest][0]

    return maxLuck

if __name__ == '__main__':
    nk = input().split()
    n = int(nk[0])
    k = int(nk[1])
    contests = []
    for _ in range(n):
        contests.append(list(map(int, input().rstrip().split())))
    result = luckBalance(k, contests)
    print(result)
