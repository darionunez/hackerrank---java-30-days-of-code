#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the isBalanced function below.
def isBalanced(s):
    opening = ['{','[','(']
    closing = ['}',']',')']

    stk = []

    for c in s:
        if (c in opening):
            stk.append(c)
        else:
            if (len(stk) <= 0):
                return "NO"

            newSymbolIndex = closing.index(c)
            topSymbolIndex = opening.index(stk[-1])

            if (newSymbolIndex == topSymbolIndex):
                stk.pop()
            else:
                return "NO"
    
    if (len(stk) > 0):
        return "NO"
        
    return "YES"

if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        s = input()
        result = isBalanced(s)
        print(result)