#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Create a dictionary of words in magazine and their frequency
# 2. Go through the note, using up words in the dictionary
# 3. If a word is not found, or is out of stock, Fail
def checkMagazine(magazine, note):
    magDic = {}

    for w in magazine:
        if w in magDic.keys():
            magDic[w] = magDic[w] + 1
        else:
            magDic[w] = 1
    
    for w in note:
        if w in magDic.keys() and magDic[w] > 0:
            magDic[w] = magDic[w] - 1
        else:
            print("No")
            return

    print("Yes")

if __name__ == '__main__':
    mn = input().split()
    m = int(mn[0])
    n = int(mn[1])
    magazine = input().rstrip().split()
    note = input().rstrip().split()
    checkMagazine(magazine, note)
