#!/bin/python3

import math
import os
import random
import re
import sys

# Convert them to sets first, then check.
def twoStrings(s1, s2):
    setS1 = set(s1)
    setS2 = set(s2)

    for i in setS1:
        if i in setS2:
            return "YES"

    return "NO"

if __name__ == '__main__':
    q = int(input())

    for q_itr in range(q):
        s1 = input()
        s2 = input()
        result = twoStrings(s1, s2)
        print(result)