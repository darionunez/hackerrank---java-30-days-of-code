#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Try to move 2 spaces
# 2. If 2 lands in a bad loud, move 1 space
# while len(c) - 1 because I don't want it to execute if it is on the last element
def jumpingOnClouds(c):
    stepsCounter = 0
    n = 0

    while n < len(c) - 1:
        if ((n + 2) < len(c) and c[n + 2] != 1):
            n = n + 2
        else:
            n = n + 1
        stepsCounter = stepsCounter + 1

    return stepsCounter;
    
if __name__ == '__main__':
    n = int(input())
    c = list(map(int, input().rstrip().split()))
    result = jumpingOnClouds(c)
    print(result)
