#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Find how many full size strings fit in n
# 2. Multiply instances of a by that number
# 3. Find the substring of the string that would complete n
# 4. Find number of instances of a in it
# 5. Add the two quantities
def repeatedString(s, n):
    prefixMultiplier = int(n / len(s))
    postfixRemainder = n % len(s)
    print(f"{prefixMultiplier} {postfixRemainder}")

    prefixCount = s.count("a") * prefixMultiplier
    postfixCount = s[:postfixRemainder].count("a")

    return prefixCount + postfixCount

if __name__ == '__main__':
    s = input()
    n = int(input())
    result = repeatedString(s, n)
    print(result)
