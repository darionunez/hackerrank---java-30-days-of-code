#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Sort the list of sock colours
# 2. Tally up pairs by looking at the index behind
# for similarity and jumping an index if a pair is found
# n + nlog(n) = n
def sockMerchantSorting(n, ar):
    pairCounter = 0 
    ar.sort()
    n = 0
    while n < len(ar):
        if n != 0:
            if ar[n-1] == ar[n]:
                pairCounter = pairCounter + 1
                print("pair found: ", ar[n-1], " ", ar[n])
                if (n+1) < len(ar):
                    n = n + 2
                    continue
        n = n + 1
    return pairCounter

# Create a frequency map from the array
# Tally up the whole number result of all keys divided by 2
# n + unique(n) = n
def sockMerchantMap(n, ar):
    frequencyCounter = {}
    pairCounter = 0
    for n in ar:
        if n not in frequencyCounter.keys():
            frequencyCounter[n] = 1
        else:
            frequencyCounter[n] = frequencyCounter[n] + 1

    for k in frequencyCounter.keys():
        pairCounter = pairCounter + int(frequencyCounter[k] / 2)
    print(frequencyCounter)
    return pairCounter

if __name__ == '__main__':
    n = int(input())
    ar = list(map(int, input().rstrip().split()))
    result = sockMerchantMap(n, ar)
    print(result)
