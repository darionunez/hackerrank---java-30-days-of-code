#!/bin/python3

import math
import os
import random
import re
import sys

# 1. Keep track of current and previous level
# 2. Whenever current level is 0 check if previous step was up
# 3. If it was up, the hiker was in a valley
def countingValleys(steps, path):
    previousLevel = 0
    currentLevel = 0
    valleyCounter = 0

    for s in path:
        if (s == "U"):
            previousLevel = currentLevel
            currentLevel = currentLevel + 1
        else:
            previousLevel = currentLevel
            currentLevel = currentLevel - 1

        if (currentLevel == 0):
            if (previousLevel == -1):
                valleyCounter = valleyCounter + 1

    return valleyCounter

if __name__ == '__main__':
    steps = int(input().strip())
    path = input()
    result = countingValleys(steps, path)
    print(result)
